#!/bin/bash

if [ $# -ne 2 ]; then
        echo "USAGE: script_name <mapped_part_name> <efi_part_name>"
        exit 1
fi

execute()
{
        echo "> $@"
        $@
}

execute mount -o subvol=@       /dev/mapper/$1 /mnt
execute mount -o subvol=@home   /dev/mapper/$1 /mnt/home
execute mount -o subvol=@cache  /dev/mapper/$1 /mnt/var/cache
execute mount -o subvol=@log    /dev/mapper/$1 /mnt/var/log
execute mount -o subvol=@root   /dev/mapper/$1 /mnt/root
execute mount -o subvol=@srv    /dev/mapper/$1 /mnt/srv
execute mount -o subvol=@tmp    /dev/mapper/$1 /mnt/tmp

execute mount /dev/$2 /mnt/boot/efi
execute mount --bind /etc/resolv.conf /mnt/etc/resolv.conf
