#!/bin/bash

execute()
{
        echo "> $@"
        $@
}

execute umount /mnt/home
execute umount /mnt/var/cache
execute umount /mnt/var/log
execute umount /mnt/root
execute umount /mnt/srv
execute umount /mnt/tmp
execute umount /mnt/boot/efi
execute umount /mnt/etc/resolv.conf

execute umount /mnt

